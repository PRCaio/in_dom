let nota = document.querySelector(".text_input")
let textarea = document.querySelector(".textarea")
let btn_enviar = document.querySelector("#btn_enviar")
let btn_calc = document.querySelector("#btn_calc_media")
let text_media = document.querySelector('.media')

var notas = []

function enviar() {
    if (parseFloat(nota.value.replace(',', '.')) >= 0 && parseFloat(nota.value.replace(',', '.')) <= 10) {
        textarea.innerHTML = textarea.innerHTML + `A nota ${notas.length + 1} foi ` + parseFloat(nota.value.replace(',', '.')) + "\n"
        notas.push(parseFloat(nota.value.replace(',', '.')))
        console.log(notas)
        nota.value = ""
    } else if( nota.value == "" ) {
        alert("Por favor, insira uma nota")
        nota.value = ""
    } else {
        alert("A nota digitada é inválida, por favor, insira uma nota válida.")
        nota.value = ""
    }
}

function calc_media(){
    var soma = 0
    for(var count = 0; count < notas.length; count ++){
        soma += notas[count]
    }
    text_media.innerText = (soma / notas.length).toFixed(2)
}

btn_enviar.addEventListener("click", () => enviar())
btn_calc.addEventListener("click", () => calc_media())