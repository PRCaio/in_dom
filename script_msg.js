let body_msg = document.querySelector(".body_msg")
let msg = document.querySelector(".textarea")
let btn_enviar = document.querySelector("#btn_enviar")

var msgNumber = 0

function enviar() {
    var number_msg = msgNumber
    body_msg.innerHTML =   `<div id="msg${number_msg}"><div id="cont_msg${number_msg}" class="msg_area"> ${msg.value} </div>` + `<div id="buttons${number_msg}" class="buttons"><div id="btn_excluir${number_msg}" class="button_editar" onclick="editar(${number_msg})" >editar</div><div id="btn_editar${number_msg}" class="button_excluir" onclick="excluir(${number_msg})" >Excluir</div></div></div>` + body_msg.innerHTML  
    msg.value = ""
    msgNumber = msgNumber + 1
}

function excluir(a) {
    var msg_ex = document.querySelector(`#msg${a}`)
    msg_ex.innerHTML = ""
}

function editar(a) {
    var msg_ex = document.querySelector(`#msg${a}`)
    var cont_msg = document.querySelector(`#cont_msg${a}`)
    msg_ex.innerHTML = `<div class="msg_area"> 
    <textarea id="msg_unic${a}" name="" id="" cols="30" rows="10" class="editar">${cont_msg.innerText}</textarea>
    </div>` + `<div class="buttons"><div  class="button_editar" onclick="salvar(${a})" >salvar</div></div>`
}

function salvar(a) {
    var msg_ex = document.querySelector(`#msg${a}`)
    var newValue = document.querySelector(`#msg_unic${a}`)

    msg_ex.innerHTML = `<div id="cont_msg${a}" class="msg_area"> ${newValue.value} </div>` + `<div id="buttons${a}" class="buttons"><div id="btn_excluir${a}" class="button_editar" onclick="editar(${a})" >editar</div><div id="btn_editar${a}" class="button_excluir" onclick="excluir(${a})" >Excluir</div></div>`
}


btn_enviar.addEventListener("click", () => enviar())
